package threading_example;

import threading_example.threading.Serwer;
import threading_example.threading.Zlecenie;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by amen on 9/11/17.
 */
public class Main {
    public static void main(String[] args) {

        Zlecenie z = new Zlecenie();
        Serwer wykonywalny = new Serwer(z);
//        Thread t = new Thread(wykonywalny);
//        t.start();
        ExecutorService watek = Executors.newSingleThreadExecutor();

        watek.submit(wykonywalny);
    }
}
