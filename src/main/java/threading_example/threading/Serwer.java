package threading_example.threading;

/**
 * Created by amen on 9/11/17.
 */
public class Serwer implements Runnable {

    private Zlecenie doWykonania;
    private boolean isRunning;

    public Serwer(Zlecenie doWykonania) {
        this.doWykonania = doWykonania;
    }

    public void run() {
        isRunning = true;
        try {
            while (isRunning) {
                System.out.println("Wykonuje zadanie.");

                System.out.println(doWykonania.a);
                System.out.println("Koncze zadanie");
                Thread.sleep(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void stop(){
        isRunning = false;
    }
}
