package washer;

import javax.swing.*;
import java.awt.*;

/**
 * Created by amen on 9/12/17.
 */
public class Window extends JFrame {

    public Window() {
        JPanel mainWindowPanel = new JPanel(new GridLayout(1, 0));
        for (int i = 0; i < 5; i++) { // dodaje 5 paneli pralki
            mainWindowPanel.add(new Washer().getMainPanel());
        }
        setContentPane(mainWindowPanel);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        pack();
    }
}
